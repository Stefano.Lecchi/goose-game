# Goose Game

Simple Java implementation of the "Goose Game".

Source code written and compiled with openjdk11.

To run and compile the game first you have to change the path where you'll keep the project in the file "build.xml" (mine is "/home/stefano/code/java/", just replace this piece of path with yours),
and then run the command "ant compile jar run", which will compile, create the jar and run the program (used ant as support to make command line build/run).

