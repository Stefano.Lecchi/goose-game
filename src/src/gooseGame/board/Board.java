package gooseGame.board;

import gooseGame.boxes.*;

import java.util.HashMap;

/**
 * Board class. Represents the board.
 * Using the singleton pattern on this class.
 */
public class Board {

    private static Board INSTANCE;
    private static int MAXBOXES = 64;

    private HashMap<Integer, Box> board;

    /**
     * Private constructor, we are using singleton pattern on this class.
     */
    private Board(){
        this.board = new HashMap<Integer, Box>();

        // Start.
        this.board.put(0, new BasicBox("Start"));

        // Setting goose boxes.
        this.board.put(5, new Goose("Goose"));
        this.board.put(9, new Goose("Goose"));
        this.board.put(14, new Goose("Goose"));
        this.board.put(18, new Goose("Goose"));
        this.board.put(23, new Goose("Goose"));
        this.board.put(27, new Goose("Goose"));

        // Setting bridge boxes.
        this.board.put(6, new Bridge("Bridge"));

        // Setting the remaining basic boxes.
        for(int i = 0; i < MAXBOXES; i++)
            if(this.board.get(i) == null)
                this.board.put(i, new BasicBox("" + i));

    }

    public static Board getInstance(){
        if(INSTANCE == null)
            INSTANCE = new Board();

        return INSTANCE;
    }

    public Box getBox(int boxNumber){

        return this.board.get(boxNumber);
    }

}
