package gooseGame.player;

import gooseGame.board.Board;
import gooseGame.game.Dice;

/**
 * Player class. Represents the player(s) playing.
 */
public class Player {

    private String playerId;
    private int position;
    private int previousPosition;
    private Dice dice;
    private Board board;

    public Player(String playerId){

        this.playerId = playerId.toLowerCase();
        this.position = 0;
        this.previousPosition = 0;
        this.dice = new Dice();
        this.board = Board.getInstance();
    }

    public int getPosition() {

        return this.position;
    }

    public void move(int newPosition) {

        this.previousPosition = this.position;
        this.position = newPosition;
    }

    public void moveByAddingSteps(int steps){

        this.previousPosition = this.position;
        this.position += steps;

        // Checking if we exceeded box 63.
        if (this.position > 63){
            this.position = 63 - (this.position - 63);
            System.out.println(this.getPlayerId() + " bounces! " + this.getPlayerId() + " returns to " + this.position);
        }

    }

    public void moveByRollingDices(){

        int firstRoll = this.dice.rollDice(1, 6);
        int secondRoll = this.dice.rollDice(1, 6);

        this.previousPosition = this.position;
        this.position += firstRoll + secondRoll;

        // Checking if we exceeded box 63.
        if (this.position > 63){

            System.out.println(this.getPlayerId() + " rolls " + firstRoll + ", " + secondRoll + " moves from " + this.board.getBox(this.getPreviousPosition()).getBoxName() + " to " + 63 + ".");

            this.position = 63 - (this.position - 63);
            System.out.println(this.getPlayerId() + " bounces! " + this.getPlayerId() + " returns to " + this.position);

            return;
        }

        System.out.println(this.getPlayerId() + " rolls " + firstRoll + ", " + secondRoll + " moves from " + this.board.getBox(this.getPreviousPosition()).getBoxName() + " to " + this.board.getBox(this.getPosition()).getBoxName());
    }

    public String getPlayerId() {

        return this.playerId;
    }

    public void setPlayerId(String playerId) {

        this.playerId = playerId;
    }

    public int getPreviousPosition() {

        return this.previousPosition;
    }

    public void moveByPrank(int newPosition){

        this.position = newPosition;
        this.previousPosition = newPosition;
    }
}
