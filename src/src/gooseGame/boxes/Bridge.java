package gooseGame.boxes;

import gooseGame.player.*;

/**
 * Bridge class. Represents the box with the image of the bridge on it.
 */
public class Bridge extends Box {

    private static int FIXEDJUMP = 12;

    public Bridge(String boxName) {
        super(boxName);
    }

    /**
     * @param player
     * Bridge box rule's moves the player to the box 12.
     */
    @Override
    public void applyRule(Player player) {

        player.move(FIXEDJUMP);
        System.out.println(player.getPlayerId() + " jumps to " + player.getPosition());
    }

    public String getBoxName(){

        return "The " + super.getName() + ". ";
    }
}
