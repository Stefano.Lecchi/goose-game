package gooseGame.boxes;

import gooseGame.player.*;

/**
 * BasicBox class. Represents a basic block, in fact, this class
 * does not apply any rule.
 */
public class BasicBox extends Box {

    public BasicBox(String boxName) {
        super(boxName);
    }

    /**
     * @param player
     * Basic box has no rules.
     */
    @Override
    public void applyRule(Player player) {
        return;
    }

    @Override
    public String getBoxName() {
        return super.getName();
    }

}
