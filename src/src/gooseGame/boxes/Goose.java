package gooseGame.boxes;

import gooseGame.player.*;

/**
 * Goose class. Represents a box with the image of the goose on it.
 */
public class Goose extends Box {

    public Goose(String boxName){
        super(boxName);
    }

    /**
     * @param player
     * Goose box rule's let the player move again (with the same amount as
     * when dices were rolled).
     */
    @Override
    public void applyRule(Player player) {

        player.move(2 * player.getPosition() - player.getPreviousPosition());
        System.out.println(player.getPlayerId() + " moves again and goes to " + player.getPosition());
    }

    @Override
    public String getBoxName(){

        return "the " + super.getName();
    }

}
