package gooseGame.boxes;

import gooseGame.player.*;

/**
 * Box abstract class. Using this to create the specific boxes.
 */
public abstract class Box {

    private String boxName;

    public Box(String boxName){

        this.boxName = boxName;
    }

    /**
     * Abstract method required to apply rules.
     * @param player
     */
    public abstract void applyRule(Player player);

    public abstract String getBoxName();

    protected String getName(){

        return this.boxName;
    }

}

