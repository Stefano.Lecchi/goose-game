package gooseGame.game;

import java.util.concurrent.ThreadLocalRandom;

public class Dice {

    private int outComingNumber;

    public Dice(){
        this.outComingNumber = 0;
    }

    public int rollDice(int bottom, int top){

        // Adding 1 to the the top number because nextInt does not include the top number of the range;
        return this.outComingNumber = ThreadLocalRandom.current().nextInt(bottom, top + 1);
    }

    public int getOutComingNumber() {
        return this.outComingNumber;
    }
}
