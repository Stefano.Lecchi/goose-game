package gooseGame.game;

import gooseGame.board.Board;
import gooseGame.player.Player;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Game class, manages the game.
 * Using singleton pattern on this class.
 */
public class Game {

    private static Game INSTANCE;
    // Game as a board.
    private Board board;
    // Game as players.
    private HashMap<String, Player> players;

    private Game() {

        this.board = Board.getInstance();
        this.players = new HashMap<String, Player>();
    }

    /**
     * @return Class' instance.
     */
    public static Game getInstance() {

        if(INSTANCE == null)
            INSTANCE = new Game();

        return INSTANCE;
    }

    /**
     * Main method, runs the game.
     */
    public void run() throws IOException {

        String userInput = "";

        BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));

        while (true) {

            userInput = buffer.readLine();

            if (this.checkExit(userInput))
                break;

            // Analyzing input.
            try{
                this.analyzeInput(userInput);
            }
            catch(Exception e){
                System.out.println(e.getMessage());
                continue;
            }

            // Checking win condition.
            if (this.checkWinCondition())
                break;

        }
    }

    /**
     * Add a player to the game. If the player is already in game,
     * the method does nothing.
     * @param player
     */
    private void addPlayer(Player player) {

        // Checking if the player isn't already in game.
        if(this.players.containsKey(player.getPlayerId())){

            System.out.println(player.getPlayerId() + ": " + "already existing player");
            return;
        }

        // Adding the player.
        this.players.put(player.getPlayerId(), player);

        // Displaying the players currently in game.
        String playersNames = "";
        Iterator iterator = this.players.entrySet().iterator();
        while (iterator.hasNext()){

            Map.Entry<String, Player> element = (Map.Entry<String, Player>)iterator.next();
            playersNames += element.getValue().getPlayerId() + "";

            if (iterator.hasNext())
                playersNames += ", ";
        }
        System.out.println("Players: " + playersNames);
    }

    /**
     * Analyzes input from the user, to understand which action we have to take.
     * @param userInput
     * @throws IllegalArgumentException
     */
    private void analyzeInput(String userInput) throws IllegalArgumentException {

        String [] parts;

        // Checking if input is an actual instruction. Also trimming input to get rid
        // of leading and trailing spaces.
        if (userInput.contains(" "))
            parts = userInput.trim().split(" ");
        else
            throw new IllegalArgumentException("String " + userInput + " is not a valid instruction");

        // If input has less than 2 words, means it's not a complete instruction.
        if (parts.length < 2)
            throw new IllegalArgumentException("String " + userInput + " is not a valid instruction");

        switch (parts [0]){

            // Checking if the user wants to add a new player
            case "add":
                // Requiring the third word which represents the name of the player.
                try{
                    this.checkAddPlayer(userInput);
                }
                catch(Exception e){
                    System.out.println(e.getMessage());
                    break;
                }
                break;

            // Checking if the user wants to move a player.
            case "move":
                try{
                    this.checkMovePlayer(userInput);
                }
                catch(Exception e){
                    System.out.println(e.getMessage());
                    break;
                }
                break;
        }
    }

    /**
     * Looks to every player, if any has arrived on position 63, he wins.
     * @return boolean value to express if a player won.
     */
    private boolean checkWinCondition() {

        for (Map.Entry<String, Player> element : this.players.entrySet())
            if (element.getValue().getPosition() == 63){
                System.out.println(element.getValue().getPlayerId() + " wins!");
                return true;
            }

        return false;
    }

    /**
     * Checks if the user wants to exit from the game.
     * @param userInput
     * @return
     */
    private boolean checkExit(String userInput) {

        if (userInput.trim().equalsIgnoreCase("exit"))
            return true;

        return false;
    }

    /**
     * Checks if the user wants to add a new player.
     * @param userInput
     * @throws IllegalArgumentException
     */
    private void checkAddPlayer(String userInput) throws IllegalArgumentException {

        String [] parts;

        // Checking if input is an actual instruction. Also trimming input to get rid
        // of leading and trailing spaces.
        if (userInput.contains(" "))
            parts = userInput.trim().split("\\s+");
        else
            throw new IllegalArgumentException("String " + userInput + " is not a valid instruction");

        if (parts.length >= 1 && parts[0].equalsIgnoreCase("add")){
            // If input has less than 3 words, means it's not a complete instruction to add a player.
            if (parts.length < 3)
                throw new IllegalArgumentException("String " + userInput + " is not a valid instruction");

            if (parts[1].equalsIgnoreCase("player"))
                addPlayer(new Player(parts[2]));
            else
                throw new IllegalArgumentException("String " + userInput + " is not a valid instruction");
        }
    }

    /**
     * Check if the user wants to move a player.
     * @param userInput
     * @throws IllegalArgumentException
     */
    private void checkMovePlayer(String userInput) throws IllegalArgumentException {

        String [] parts;

        parts = userInput.trim().split("\\s+");

        // If input has less than 2 words, means it's not a complete instruction.
        if (parts.length < 2)
            throw new IllegalArgumentException("String " + userInput + " is not a valid instruction");

        // Checking if the user wants to move a player.
        if (this.players.get(parts[1]) != null){

            // Moving the player.
            Player movingPlayer = this.players.get(parts[1]);
            movingPlayer.moveByRollingDices();

            // Prank.
            this.prank(movingPlayer);

            // If any, applying rules related to the box.
            this.board.getBox(movingPlayer.getPosition()).applyRule(movingPlayer);

            // Prank. Need to check twice because of possible steps forward due to boxes' rules.
            this.prank(movingPlayer);
        }
        else
            throw new IllegalArgumentException("Player " + parts[1] + " is not a valid player");
    }

    /**
     * Prank. If the moving player finishes on a box previously with another player on it,
     * the moving player sends the other player(s) to the box he arrived from.
     * @param movingPlayer
     */
    private void prank(Player movingPlayer){

        // Iterate through the players, if any have the same position of the moving player, apply prank.
        for (Map.Entry<String, Player> player : this.players.entrySet()){

            if (player.getValue().getPosition() == movingPlayer.getPosition() && player.getValue().getPlayerId() != movingPlayer.getPlayerId()) {

                player.getValue().moveByPrank(movingPlayer.getPreviousPosition());
                System.out.println("On " + movingPlayer.getPosition() + " there is " + player.getValue().getPlayerId() + ", who returns to " + player.getValue().getPosition());
            }

        }
    }

}
